import React from 'react';
import { StyleSheet, Text, View, ScrollView  } from 'react-native';

import SideMenu from 'react-native-side-menu';

import List from './components/List';
import Slider from './components/Slider';
import Header from './components/Header';
import Menu from './components/Menu';

export default class App extends React.Component {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	isOpen: false,
	  	itemSelected: 'Start',
	  };
	  this.itemSelected = this.itemSelected.bind(this);
	}	
  	toggle(){
	 	this.setState({
	  		isOpen: !this.state.isOpen,
	  	});
	}
	itemSelected(item){
        this.setState({
            itemSelected: item,
            isOpen: false,
        });
    }
	updateMenu(isOpen){
		this.setState({isOpen});
	}
  render() {
    return (
      <View style={styles.container}>
	      <SideMenu
	      	menu={<Menu 
   			itemSelected={this.itemSelected} 
			itemSelectedValue={this.state.itemSelected}
			/>}
	      	isOpen={this.state.isOpen}
	      	onChange={(isOpen)=> this.updateMenu(isOpen)}

	      >
	      <Header toggle={this.toggle.bind(this)}/>
	      <ScrollView style={{backgroundColor: '#fff'}}>
		      	
		      	<Slider />
		       	<List />
		  </ScrollView>
	       </SideMenu>
      </View>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: '100%',
		backgroundColor: '#fff',
	}
});