import React, {Component} from 'react';
import { Dimensions, StyleSheet, View, Text, Image, ScrollView, TouchableHighlight } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const {width, height} = Dimensions.get('window');

class Menu extends Component{
	_renderItemsMenu(){
        const genres = ["Start", "Biznes", "Rozrywka", "Zdrowie", "Nauka", "Sport", "Technologia"];
        const {itemSelectedValue} = this.props;
        return genres.map((element, key) => (
            <TouchableHighlight 
                key={key}
                style={element == itemSelectedValue ? [styles.items, styles.itemSelected]: styles.noSelectedItems}
                onPress={() => this.props.itemSelected(element)}    
            >
                <Text style={styles.text}>{element}</Text>
            </TouchableHighlight>
        ))
    }
	render(){
		return(
			<View style={styles.menu}>
                <ScrollView style={styles.scrollContainer}>
                    <Text style={styles.title}>Kategorie</Text>
                    {this._renderItemsMenu()}
                    <Text style={styles.title}>Informacje</Text>
                    <Text style={styles.footer}>Autor: Marcin Pacholarz</Text>
                    <Text style={styles.footer}>GitHub: https://github.com/marlarz</Text>
                    <Text style={styles.footer}>LinkedIn: https://www.linkedin.com/in/pacholarz/</Text>
                </ScrollView>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: width,
        height: height,
        backgroundColor:"#fff",
    },
    text: {
        color: '#a3a3a3',
        fontSize: 15,
    },
    scrollContainer: {
    	marginTop: 40,
        width: width / 2 + 59,
    },
    items: {
        paddingVertical: 15,
        paddingLeft: 20,
    },
    itemSelected:{
        borderLeftWidth: 5,
        borderColor: 'red',
    },
    noSelectedItems: {
        paddingVertical: 15,
        paddingLeft: 25,
        marginTop: 5,
    },
    title: {
    	fontSize: 16,
    	fontWeight: 'bold',
    	color: 'black',
    	padding: 15,
    	backgroundColor: '#eee',
    },
    footer:{
    	fontSize: 13,
    	color: '#ccc',
    	paddingTop: 5,
    	textAlign: 'center',
    },
});

export default Menu;