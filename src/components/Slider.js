import React from 'react';
import { Text, View, Image, StyleSheet, Dimensions} from 'react-native';

import Swiper from 'react-native-swiper';

const {width} = Dimensions.get('window');

const articles = [{"source":{"id":null,"name":"Interia.pl"},"author":null,"title":"Dolnośląskie: W kajdankach uciekł z konwoju","description":"Kliknij i zobacz więcej.","url":"https://fakty.interia.pl/dolnoslaskie/news-dolnoslaskie-w-kajdankach-uciekl-z-konwoju,nId,2616471","urlToImage":"https://i.iplsc.com/-/0006ML7ZVW070F4B-C411.jpg","publishedAt":"2018-08-08T13:08:20Z"},{"source":{"id":null,"name":"Polskieradio.pl"},"author":null,"title":"Łódzkie: postępowanie w sprawie pogryzienia przez psa 8–letniej dziewczynki","description":"Prokuratura w Zgierzu wyjaśnia okoliczności pogryzienia przez psa 8–letniej dziewczynki. Dziecko z rozległymi ranami szarpanymi twarzy trafiło do jednego z łódzkich szpitali - poinformował w środę PAP rzecznik Prokuratury Okręgowej w Łodzi Krzysztof Kopania.","url":"https://www.polskieradio.pl/5/3/Artykul/2176357,Lodzkie-postepowanie-w-sprawie-pogryzienia-przez-psa-8%E2%80%93letniej-dziewczynki","urlToImage":"http://static.prsa.pl/images/c57f5238-5f2c-4028-8c67-2d9edb8a4852.jpg","publishedAt":"2018-08-08T13:03:00Z"},{"source":{"id":null,"name":"Wiadomosci.wp.pl"},"author":"oprac. Maciej Deja","title":"PiS przejmie też sejmiki. Najnowszy sondaż","description":"PiS wygra w wybory do sejmików w 10 na 16 województw, a koalicja Platformy Obywatelskiej i Nowoczesnej tylko w czterech - wynika z sondażu IBRiS zleconego przez SLD. Tym samym partia rządząca odbiła 7 województw,","url":"https://wiadomosci.wp.pl/pis-przejmie-tez-sejmiki-najnowszy-sondaz-6282168490662017a","urlToImage":"https://d.wpimg.pl/1922470057--426058191/jaroslaw-kaczynski-sejm.jpg","publishedAt":"2018-08-08T12:53:48Z"},{"source":{"id":null,"name":"Money.pl"},"author":"Adam Janczewski, oprac. Adam Janczewski","title":"Pożar w fabryce płyt wiórowych w Mielcu. Kronospan szacuje straty","description":"W fabryce koncernu Kronospan w Mielcu na Podkarpaciu doszło do dużego pożaru. Dym widoczny był ze znacznej odległości. Z ogniem walczyło kilkanaście zastępów straży. Firma na razie nie chce informować o stratach i przyczynach. Przyznaje jedynie, że ogień poja…","url":"https://www.money.pl/gospodarka/wiadomosci/artykul/pozar-w-fabryce-plyt-wiorowych-w-mielcu,138,0,2412938.html","urlToImage":"https://money.wpcdn.pl/i/h/246/oryg425974.jpg","publishedAt":"2018-08-08T12:12:23Z"},{"source":{"id":null,"name":"Wprost.pl"},"author":null,"title":"Od 17 lat siedzi w więzieniu, choć jest niewinny. Sąd nie zgodził się na przedterminowe zwolnienie","description":"Dziennikarze Onetu po raz kolejny wracają do przypadku Adama Dudały. Mężczyzna od 17 lat siedzi w więzieniu, choć ze śledztwa przeprowadzonego przez reporterów wynika, że jest niewinny. Ostatnio sąd odmówił Dudale przedterminowego zwolnienia z zakładu karnego.","url":"https://www.wprost.pl/kraj/10144798/od-17-lat-siedzi-w-wiezieniu-choc-jest-niewinny-sad-nie-zgodzil-sie-na-przedterminowe-zwolnienie.html","urlToImage":"https://www.wprost.pl/_thumb/87/41/0e0fb0b040958296024d398dcd47.jpeg","publishedAt":"2018-08-08T11:28:22Z"},{"source":{"id":null,"name":"Fakt.pl"},"author":"snow, Daily Mail","title":"10-miesięczna dziewczynka zmarła z głodu i odwodnienia. Rodzice nie chcieli jej leczyć","description":"Para 27-latków pozwoliła umrzeć z głodu i odwodnienia swojej 10-miesięcznej córce. Rodzice od miesiąca widzieli, że dziewczynka traci na wadze i jest coraz słabsza, ale pozwolili jej umrzeć, bo \"Bóg t...","url":"https://www.fakt.pl/wydarzenia/swiat/10-miesieczna-mary-zmarla-z-glodu-i-odwodnienia-rodzice-nie-chcieli-jej-leczyc/z7yfskv","urlToImage":"https://ocdn.eu/pulscms-transforms/1/AeektkqTURBXy80MTdiYWRiYjVhZTE3YWJkMmQ4ZGViZDQwNTQ5ZTgxZS5qcGVnkpUDAADNAnrNAWWTBc0B4M0BaA","publishedAt":"2018-08-08T11:26:32Z"},{"source":{"id":null,"name":"Gazetaprawna.pl"},"author":null,"title":"Komenda: Chciałbym, by śledztwo się już skończyło, abym mógł normalnie żyć","description":"Przesłuchania w prokuraturze dużo mnie kosztują. Chcę, by winni zostali ukarani, ale też tego, by śledztwo już się skończyło, abym mógł normalnie żyć – powiedział w środę, przed kolejnym przesłuchaniem we wrocławskiej prokuraturze, Tomasz Komenda.","url":"http://www.gazetaprawna.pl/artykuly/1208733,tomasz-komenda-o-przesluchaniach-w-prokuraturze.html","urlToImage":"http://g3.gazetaprawna.pl/p/_wspolne/pliki/3361000/3361738-657-323.jpg","publishedAt":"2018-08-08T10:42:00Z"},{"source":{"id":null,"name":"Gazetawroclawska.pl"},"author":"PB, JJ","title":"Wypadek na A4: Ciężarówka przebiła barierki i leży w poprzek drogi. Kruszywo i olej napędowy na jezdni [ZDJĘCIA]","description":"&nbsp;","url":"http://www.gazetawroclawska.pl/motofakty/a/wypadek-na-a4-ciezarowka-przebila-barierki-i-lezy-w-poprzek-drogi-kruszywo-i-olej-napedowy-na-jezdni-zdjecia,13399421/","urlToImage":"https://d-pt.ppstatic.pl/k/r/1/b2/ac/5b6aab3e85ff9_o.jpg?1533730322","publishedAt":"2018-08-08T10:26:00Z"},{"source":{"id":null,"name":"Pomponik.pl"},"author":null,"title":"Pogrzeb Kory. Rodzina, przyjaciele i fani żegnają artystkę. Wzruszająca przemowa Kamila Sipowicza","description":"Kliknij i zobacz więcej.","url":"http://www.pomponik.pl/plotki/news-pogrzeb-kory-rodzina-przyjaciele-i-fani-zegnaja-artystke-wzr,nId,2616322","urlToImage":"https://i.iplsc.com/-/0007J0C3PDN8530Q-C411.jpg","publishedAt":"2018-08-08T10:22:25Z"},{"source":{"id":null,"name":"Wiadomosci.wp.pl"},"author":"Tomasz Molga","title":"Idzie nowy armageddon. Burzowe \"chmury wieżowce\", przed którymi ostrzegają lotnicy","description":"Po fali upałów nad Polskę nadciąga kolejne pogodowe zjawisko. Gigantyczne burzowe chmury cumulonimbus , wypiętrzone na ponad 10 km.  To \"chmury wieżowce\", przed którymi już ostrzegają się lotnicy. Kto planuje","url":"https://wiadomosci.wp.pl/idzie-nowy-armageddon-burzowe-chmury-wiezowce-przed-ktorymi-ostrzegaja-lotnicy-6282118701758081a","urlToImage":"https://d.wpimg.pl/368986606-1522404297/burze-pogoda.jpg","publishedAt":"2018-08-08T10:07:38Z"},{"source":{"id":null,"name":"Onet.pl"},"author":null,"title":"Rzecznik SN o \"potencjalnej odpowiedzialności prezydenta przed Trybunałem Stanu\"","description":null,"url":"https://wiadomosci.onet.pl/kraj/michal-laskowski-o-potencjalnej-odpowiedzialnosci-prezydenta-rp-przed-ts/xh406em","urlToImage":null,"publishedAt":"2018-08-08T09:22:05Z"},{"source":{"id":null,"name":"Tvn24bis.pl"},"author":null,"title":"Zmian w 500 plus nie będzie. Szydło: lepsze jest wrogiem dobrego","description":"Program 500 plus to ogromny sukces, będzie kontynuowany w obecnej formule. Dyskusja o jego zmianie jest dziś niepotrzebna. Lepsze jest wrogiem dobrego - napisała na Twitterze wicepremier, szefowa Komitetu Społecznego Rady Ministrów Beata Szydło.","url":"https://tvn24bis.pl/z-kraju,74/szydlo-program-500-plus-bedzie-kontynuowany-w-obecnej-formule,859580.html","urlToImage":"https://r-scale-b9.dcs.redcdn.pl/scale/o2/tvn/web-content/m/p1/i/3e15cc11f979ed25912dff5b0669f2cd/1667f16e-04d0-4717-8a2e-bdbb5f44b0c4.jpg?type=1&srcmode=4&srcx=0%2F1&srcy=0%2F1&srcw=560&srch=340&dstw=560&dsth=340&quality=75","publishedAt":"2018-08-08T08:51:00Z"},{"source":{"id":null,"name":"Eska.pl"},"author":null,"title":"Prognoza pogody 8.08.2018 - nowe ostrzeżenia IMGW! Gdzie najgoręcej? [WOJEWÓDZTWA]","description":"Wysokie temperatury utrzymują się w Polsce! Na ESKA.pl dowiecie się, w których województwach obowiązują ostrzeżenia IMGW II i III stopnia.","url":"https://www.eska.pl/news/prognoza-pogody-8-08-2018-nowe-ostrzezenia-imgw-gdzie-najgorecej-wojewodztwa-aa-3zMh-C6WJ-MwBq.html","urlToImage":null,"publishedAt":"2018-08-08T08:47:26Z"},{"source":{"id":null,"name":"Sport.pl"},"author":"Sport.pl","title":"Bundesliga. Niko Kovac: Lewandowski zaakceptował to, że zostaje w Bayernie","description":"- Lewandowski jest jednym z trzech najlepszych zawodnik�w na �wiecie wyst�puj�cych na tej pozycji. Dlatego z pewno�ci� go nie oddamy. To stanowisko, kt�re mu przekaza�em. Robert to zaakceptowa� - na �amach \"Sport Bildu\" powiedzia� trener Bayernu Monachium, Ni…","url":"http://www.sport.pl/pilka/7,65081,23764593,bundesliga-niko-kovac-lewandowski-zaakceptowal-to-ze-zostaje.html","urlToImage":"http://bi.gazeta.pl/im/51/44/16/z23350865IER.jpg","publishedAt":"2018-08-08T08:39:00Z"},{"source":{"id":null,"name":"Interia.pl"},"author":null,"title":"Hanna Gronkiewicz-Waltz: Czekam na doprowadzanie mnie siłą przed komisję weryfikacyjną","description":"Kliknij i zobacz więcej.","url":"https://fakty.interia.pl/raporty/raport-afera-reprywatyzacyjna-w-warszawie/aktualnosci/news-hanna-gronkiewicz-waltz-czekam-na-doprowadzanie-mnie-sila-pr,nId,2616265","urlToImage":"https://i.iplsc.com/-/000728P2K2LTDF0Q-C411.jpg","publishedAt":"2018-08-08T08:11:45Z"},{"source":{"id":null,"name":"Newsweek.pl"},"author":"KOW","title":"Rozrzutny jak marszałek Kuchciński. Na zakup dwóch ekranów Marszałek chce wydać 2 mln złotych","description":"Marszałek Marek Kuchciński planuje wymianę ekranów, które na sali plenarnej pokazują wyniki głosowań. W projekcie budżetu Kancelarii Sejmu na 2019 rok zapisano, że na dwie nowe tablice elektroniczne zostanie przeznaczonych aż 2 mln złotych. Opozycja próbowała…","url":"http://www.newsweek.pl/polska/polityka/kuchcinski-planuje-zakup-dwoch-ekranow-do-sejmu-za-2-mln-zlotych,artykuly,431201,1.html","urlToImage":"http://ocdn.eu/ebooksimages-transforms/1/ZHpktoARGh0dHA6Ly9vY2RuLmV1L25ld3N3ZWVrLXdlYi8wZTlkOGQyNi1lYjliLTRhZmItYWYzYi01ZTQ0NDY5NzY0ZjAuanBnkpMFzQSwzQJYlQfaAEBodHRwOi8vb2Nkbi5ldS9uZXdzd2Vlay13ZWIvZDU1ZTIxYmU5ODg0NmQ3ODU0NjkwODNiZDc0Y2U4ZGUucG5nCcIA","publishedAt":"2018-08-08T08:10:12Z"},{"source":{"id":null,"name":"Tvn24.pl"},"author":null,"title":"Rok temu awansów nie było. Teraz prezydent wręczy 11 nominacji generalskich","description":"W Święto Wojska Polskiego, 15 sierpnia, prezydent Andrzej Duda wręczy 11 nominacji generalskich, po pięc na stopień generała brygady i generała dywizji ora...","url":"https://www.tvn24.pl/wiadomosci-z-kraju,3/andrzej-duda-wreczy-nominacje-generalskie,859545.html","urlToImage":"https://r-scale-c0.dcs.redcdn.pl/scale/o2/tvn/web-content/m/p1/i/61b1fb3f59e28c67f3925f3c79be81a1/93886c10-7028-4333-a1ed-c2fe1c2f49a0.jpg?type=1&srcmode=4&srcx=0/1&srcy=0/1&srcw=640&srch=2000&dstw=640&dsth=2000&quality=80","publishedAt":"2018-08-08T05:55:50Z"},{"source":{"id":null,"name":"Wiadomosci.wp.pl"},"author":"oprac. Magdalena Nałęcz","title":"Nie żyje 12-latek, którego na kempingu przygniotło drzewo. Wszczęto śledztwo","description":"W szpitalu w Gorzowie Wielkopolskim zmarł 12-latek. Lekarze walczyli o życie chłopca od czwartku. Nastolatek został przygnieciony przez drzewo, które spadło na przyczepę kempingową w Osieku.","url":"https://wiadomosci.wp.pl/nie-zyje-12-latek-ktorego-na-kempingu-przygniotlo-drzewo-wszczeto-sledztwo-6281915282990721a","urlToImage":"https://d.wpimg.pl/624997529--982653164/kemping.jpg","publishedAt":"2018-08-07T19:51:57Z"},{"source":{"id":null,"name":"Www.rp.pl"},"author":null,"title":"Milion złotych kary za pożar śmieci w Zgierzu","description":"Milion złotych będzie musiała zapłacić firma Green-Tec Solutions za pożar, który pod koniec maja wybuchł na składowisku odpadów w Zgierzu. Strażacy musieli walczyć z ogniem przez kilka dni.","url":"https://www.rp.pl/Spoleczenstwo/180809560-Milion-zlotych-kary-za-pozar-smieci-w-Zgierzu.html","urlToImage":"https://www.rp.pl/storyimage/RP/20180807/KRAJ/180809560/AR/0/AR-180809560.jpg?minW=200&minH=200&exactW=600&exactH=351&exactFit=crop","publishedAt":"2018-08-07T15:46:00Z"},{"source":{"id":null,"name":"Wpolityce.pl"},"author":null,"title":"Patryk Jaki ruszył z akcją \"100imy pod blokiem\": Warszawa to nie tylko wielkie biurowce i korporacje","description":"\"Rozpoczynamy dziś akcję spotkań z mieszkańcami warszawskich dzielnic, dziś Bielan\".","url":"https://wpolityce.pl/polityka/406911-jaki-ruszyl-z-akcja-100imy-pod-blokiem","urlToImage":"https://media.wplm.pl/pictures/2018/08/06/825/360/4b55c134b8bd4539ad7ff07bbfbe2205.jpeg","publishedAt":"2018-08-06T19:57:00Z"}];

const Slid = props => {
    return(
		<View style={styles.container}>
	        <Image style={styles.image} source={{uri: props.uri.urlToImage}}/>
	        <View style={styles.textContainer}>
	        	<Text style={styles.text}>{props.uri.title}</Text>
	        	<Text style={styles.pubAt}>{props.uri.description.substring(0, 150)} ...</Text>
	        </View>
	    </View>
    );
}

export default class Slider extends React.Component{
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	sliderData: [ articles[1], articles[2], articles[3]
	  	],
	  };
	}
	render(){
        return (
            <View style={styles.swiperContainer}>
                <Swiper
                    autoplay
                   	autoplayTimeout={5}
                    showsButtons={true}
                    showsPagination={false}
                >
                {
                    this.state.sliderData.map((item, i) => <Slid 
                        uri={item}
                        key={i}
                    />)
                }

                </Swiper>
            </View>
        );
    }
}

const styles = StyleSheet.create({
	swiperContainer:{
		height: 310,
	},
	container: {
        flex: 1,
        justifyContent: 'center',
    },
    image: {
        flex: 4,
        width,
    },
    textContainer:{
    	flex: 2,
    	padding: 15,
    },
    text:{
    	fontSize: 18,
    	color: 'black',
    	fontWeight: 'bold',
    	lineHeight: 18,
    },
    pubAt:{
    	color: '#9b9b9b',
    	fontSize: 12,
    	marginTop: 5,
    },
    addBy:{
    	color: '#9b9b9b',
    	fontSize: 16,
    },
});