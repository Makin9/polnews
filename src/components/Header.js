import React from 'react';
import { Text, View, StyleSheet, Image, TouchableWithoutFeedback } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const Header = props => (
	<View style={styles.container}>
		<TouchableWithoutFeedback onPress={()=> props.toggle()}>
			<Icon
				name="bars"
				color="white"
				size={25}
				style={styles.icons}
			/>
		</TouchableWithoutFeedback>
		<Image style={styles.logo} source={require('../images/logo.png')} />
		<Icon
			name="search"
			color="white"
			size={25}
			style={styles.icons}
		/>
	</View>
);

const styles = StyleSheet.create({
	container: {
		paddingTop: 23,
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: '#c0392b',
		paddingHorizontal: 0,
	},
	logo:{
		height: 50,
		width: 160,
	},
	icons:{
		padding: 15,
	},
});

export default Header;