## POLSKIE WIADOMOŚCI

#### Screenshoots

![](https://bitbucket.org/Makin9/polnews/raw/775009e34f2193c972afbe9733fbcac27149c065/ss/ss1.png)

Slider menu:

![](https://bitbucket.org/Makin9/polnews/raw/775009e34f2193c972afbe9733fbcac27149c065/ss/ss2.png)

Wymaga:
- Node
```sh
npm install -g create-react-native-app
```

Uruchomienie
```sh
cd nazwaProjektu
npm start
```